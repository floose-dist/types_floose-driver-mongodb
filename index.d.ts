import {Framework, Utils} from "floose";
import Driver = Framework.Driver;
import Repository = Framework.Data.Repository;
import Model = Framework.Data.Model;
import {Collection} from "mongodb";
import SearchCriteria = Framework.Data.SearchCriteria;
import Join = Framework.Data.Join;
import Schema = Utils.Validation.Schema;

export declare class ObjectId {
    /**
     * Create a new ObjectId instance
     * @param {(string|number|ObjectId)} id Can be a 24 byte hex string, 12 byte binary string or a Number.
     */
    constructor(id?: string | number | ObjectId);
    /** The generation time of this ObjectId instance */
    generationTime: number;
    /** If true cache the hex string representation of ObjectId */
    static cacheHexString?: boolean;
    /**
     * Creates an ObjectId from a hex string representation of an ObjectId.
     * @param {string} hexString create a ObjectId from a passed in 24 byte hexstring.
     * @return {ObjectId} return the created ObjectId
     */
    static createFromHexString(hexString: string): ObjectId;
    /**
     * Creates an ObjectId from a second based number, with the rest of the ObjectId zeroed out. Used for comparisons or sorting the ObjectId.
     * @param {number} time an integer number representing a number of seconds.
     * @return {ObjectId} return the created ObjectId
     */
    static createFromTime(time: number): ObjectId;
    /**
     * Checks if a value is a valid bson ObjectId
     *
     * @return {boolean} return true if the value is a valid bson ObjectId, return false otherwise.
     */
    static isValid(id: string | number | ObjectId): boolean;
    /**
     * Compares the equality of this ObjectId with `otherID`.
     * @param {ObjectId|string} otherID ObjectId instance to compare against.
     * @return {boolean} the result of comparing two ObjectId's
     */
    equals(otherID: ObjectId | string): boolean;
    /**
     * Generate a 12 byte id string used in ObjectId's
     * @param {number} time optional parameter allowing to pass in a second based timestamp.
     * @return {string} return the 12 byte id binary string.
     */
    static generate(time?: number): Buffer;
    /**
     * Returns the generation date (accurate up to the second) that this ID was generated.
     * @return {Date} the generation date
     */
    getTimestamp(): Date;
    /**
     * Return the ObjectId id as a 24 byte hex string representation
     * @return {string} return the 24 byte hex string representation.
     */
    toHexString(): string;
}

export declare interface MongodbDriverConfig {
    host: string,
    port: number,
    database: string,
    user?: string,
    password?: string,
    options?: {}
}

export declare class MongodbDriver extends Driver {
    readonly configurationValidationSchema: Schema;
    init<T>(config: T): Promise<void>;
    connect(): Promise<void>;
    close(): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Db.html#createCollection */
    createCollection(collection: string, options: {[index: string]: any}): Promise<Collection<any>>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Db.html#createIndex */
    createIndex(index: string, fieldOrSpec: string | {[index: string]: any}, options: {[index: string]: any}): Promise<void>;
    /** https://docs.mongodb.com/manual/core/schema-validation/ */
    addValidation(collection: string, validatorSchema: {[index: string]: any}, validationLevel?: "off" | "strict" | "moderate"): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#save */
    save(collection: string, object: {[index: string]: any}, options?: {[index: string]: any}): Promise<void | ObjectId>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#updateOne */
    updateOne(collection: string, filter: {[index: string]: any}, update: {[index: string]: any}, options?: {[index: string]: any}): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#updateMany */
    updateMany(collection: string, filter: {[index: string]: any}, update: {[index: string]: any}, options?: {[index: string]: any}): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#insertOne */
    insertOne(collection: string, object: {[index: string]: any}, options?: {[index: string]: any}): Promise<ObjectId>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#insertMany */
    insertMany(collection: string, objects: {[index: string]: any}[], options?: {[index: string]: any}): Promise<{[key: number]: ObjectId}>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#deleteOne */
    deleteOne(collection: string, filter: {[index: string]: any}, options?: {[index: string]: any}): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#deleteMany */
    deleteMany(collection: string, filter: {[index: string]: any}, options?: {[index: string]: any}): Promise<void>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#findOne */
    findOne(collection: string, query: {[index: string]: any}, projection?: {[index: string]: any}): Promise<{[index: string]: any}>;
    /** http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html#find */
    findMany(collection: string, query: {[index: string]: any}, options?: {[index: string]: any}): Promise<{[index: string]: any}[]>;
    /** http://mongodb.github.io/node-mongodb-native/3.0/api/Collection.html#aggregate */
    aggregate(collection: string, pipeline: {[index: string]: any}[], options?: {[index: string]: any}): Promise<{[index: string]: any}[]>;
}

export declare abstract class AbstractMongodbModel extends Model {
    createdAt: Date;
    updatedAt: Date;
}

export declare abstract class AbstractMongodbRepository<T extends AbstractMongodbModel> extends Repository<T> {
    protected _connection: string;
    constructor(connection: string);

    protected abstract _collectionName(): string;
    protected abstract _eventPrefix(): string;
    protected abstract _modelClass(): {new(): T};

    protected _find(searchCriteria: SearchCriteria, joinCriteria?: Join[]): Promise<T[]>;

    create(): T;
    get(identifier: string | number | ObjectId, forceReload?: boolean): Promise<T>;
    save(modelInterface: T): Promise<T>;
    delete(modelInterface: T | T[]): Promise<void>;
}